import { createStore } from 'vuex';

import auth from './auth';
import main from './main';

export default createStore({
  modules: {
    auth, main,
  },
});
