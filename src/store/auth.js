export default {
  namespaced: true,
  state: {
    online: 200,
    maxOnline: 500,
    playerData: {},
    // headList: [
    //   { title: 'Обновления', page: 'updates' },
    //   { title: 'Анонс обновлений', page: 'announces' },
    //   { title: 'Список изменений', page: 'changes' },
    // ],
    headList: [
      { title: 'Играть', page: 'play' },
      { title: 'Магазин', page: 'shop' },
      { title: 'Персонаж', page: 'character' },
      { title: 'Фракция', page: 'fraction' },
      { title: 'Дуэли', page: 'duels' },
      { title: 'Другое', page: 'misc' },
    ],
    gameModes: [
      {
        id: 1,
        title: 'Создать лобби',
        icon: 'duo',
        type: 'Multi',
        place: 'Все локации',
      },
    ],
    settings: {
      chat: {
        chatHeight: 20,
        chatWidth: 20,
        fontSize: 20,
        lineHeight: 20,
        chatOpacity: 20,
      },
    },
    teams: {
      members: {
        progress: 2,
        players: [
          {
            icon: 'https://i.picsum.photos/id/668/49/49.jpg?hmac=q5RVYmHAfRPhfT693oxx_KKSbjpCSX-Dux6gJKqx6z4',
          },
        ],
      },

      opponent: {
        progress: 5,
        players: [],
      },
    },
    chat: [
      'text',
      'text',
      'text',
    ],
    updates: [
      {
        title: 'Новое обновление',
        date: '09.04.2021, 20:30',
        picture: 'event',
        text: `Cum urna nec, consequat elit nisl, ante ac sit urna nunc vitae tristique 
        non habitasse urna at fringilla molestie vestibulum urna nec, consequat 
        elit nisl, ante ac sit urna nunc vitae tristique non habitasse urna at fringilla 
        molestie vestibulum urna nec, consequat elit nisl, ante ac sit urna nunc 
        vitae tristique non habitasse urna at fringilla molestie vestibulum urna nec, 
        consequat elit nisl, ante ac sit urna nunc vitae tristique non habitasse 
        urna at fringilla molestie vestibulum`,
      },

      {
        title: 'Новое обновление',
        date: '09.04.2021, 20:30',
        picture: 'event',
        text: `Cum urna nec, consequat elit nisl, ante ac sit urna nunc vitae tristique 
        non habitasse urna at fringilla molestie vestibulum urna nec, consequat 
        elit nisl, ante ac sit urna nunc vitae tristique non habitasse urna at fringilla 
        molestie vestibulum urna nec, consequat elit nisl, ante ac sit urna nunc 
        vitae tristique non habitasse urna at fringilla molestie vestibulum urna nec, 
        consequat elit nisl, ante ac sit urna nunc vitae tristique non habitasse 
        urna at fringilla molestie vestibulum`,
      },

      {
        title: 'Новое обновление',
        date: '09.04.2021, 20:30',
        picture: 'event',
        text: `Cum urna nec, consequat elit nisl, ante ac sit urna nunc vitae tristique 
        non habitasse urna at fringilla molestie vestibulum urna nec, consequat 
        elit nisl, ante ac sit urna nunc vitae tristique non habitasse urna at fringilla 
        molestie vestibulum urna nec, consequat elit nisl, ante ac sit urna nunc 
        vitae tristique non habitasse urna at fringilla molestie vestibulum urna nec, 
        consequat elit nisl, ante ac sit urna nunc vitae tristique non habitasse 
        urna at fringilla molestie vestibulum`,
      },

      {
        title: 'Новое обновление',
        date: '09.04.2021, 20:30',
        picture: 'event',
        text: `Cum urna nec, consequat elit nisl, ante ac sit urna nunc vitae tristique 
        non habitasse urna at fringilla molestie vestibulum urna nec, consequat 
        elit nisl, ante ac sit urna nunc vitae tristique non habitasse urna at fringilla 
        molestie vestibulum urna nec, consequat elit nisl, ante ac sit urna nunc 
        vitae tristique non habitasse urna at fringilla molestie vestibulum urna nec, 
        consequat elit nisl, ante ac sit urna nunc vitae tristique non habitasse 
        urna at fringilla molestie vestibulum`,
      },
    ],
    lobbies: [
      {
        title: 'Создать лобби',
        img: 'https://i.picsum.photos/id/912/136/110.jpg?hmac=FvYvJgRbbkbdAMPML4jd8TLG7foed7CyYXlDj8CVoKY',
        type: 'Multi',
        place: 'Все локации',
      },
    ],
    donate: {
      categories: [
        { title: 'Название категории', path: 'category-1' },
        { title: 'Название категории', path: 'category-2' },
        { title: 'Название категории', path: 'category-3' },
      ],
      categoryItems: [
        {
          title: 'Товар',
          category: 'Категория',
          price: 50,
          description: 'Nibh etiam fames consectetur proin neque Nibh etiam fames consectetur proin neque Nibh etiam fames consectetur proin neque Nibh etiam fames consectetur proin neque Nibh etiam fames consectetur proin neque',
        },
      ],
    },
  },

  mutations: {
    updateChat(state, data) {
      const parsedData = JSON.parse(data);
      state.chat.push(...parsedData);
    },

    updateSettings(state, data) {
      const parsedData = JSON.parse(data);
      state.settings[parsedData.page][parsedData.field] = parsedData.value;
    },
  },
};
