import { createRouter, createWebHashHistory } from 'vue-router';

import Auth from '@/pages/auth/auth.vue';

const routes = [
  {
    path: '/auth',
    name: 'auth',
    component: Auth,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
