import { useStore } from 'vuex';
import {
  ref, computed, watch, nextTick, onMounted, reactive,
} from 'vue';

import NewsCard from './news-card.vue';

export default {
  name: 'auth',

  components: {
    NewsCard,
  },

  setup() {
    const store = useStore();
    const dropShowed = ref(false);
    const currentPage = ref('shop');
    const login = ref(null);
    const password = ref(null);
    const repeatPassword = ref(null);
    const email = ref(null);
    const promo = ref(null);
    const authMethod = ref('auth');
    const playerId = ref(null);
    const message = ref(null);
    const messagesContainer = ref(null);
    const settingsPage = ref('chat');
    const shopPage = ref(null);
    const selectedShopItem = ref(null);

    const chatSettings = reactive([
      { title: 'Высота чата', model: null, name: 'chatHeight' },
      { title: 'Ширина чата', model: null, name: 'chatWidth' },
      { title: 'Размер шрифта', model: null, name: 'fontSize' },
      { title: 'Расстояние между строк', model: null, name: 'lineHeight' },
      { title: 'Прозрачность чата', model: null, name: 'chatOpacity' },
    ]);

    // store
    const online = computed(() => store.state.auth.online);
    const maxOnline = computed(() => store.state.auth.maxOnline);
    const updates = computed(() => store.state.auth.updates);
    const playerData = computed(() => store.state.auth.playerData);
    const headList = computed(() => store.state.auth.headList);
    const gameModes = computed(() => store.state.auth.gameModes);
    const teams = computed(() => store.state.auth.teams);
    const chat = computed(() => store.state.auth.chat);
    const settings = computed(() => store.state.auth.settings);
    const lobbies = computed(() => store.state.auth.lobbies);
    const donate = computed(() => store.state.auth.donate);

    const changeHeadDropState = () => {
      dropShowed.value = !dropShowed.value;
    };

    const setSettingsPage = (page) => {
      settingsPage.value = page;
    };

    const setPage = (page) => {
      currentPage.value = page;
      selectedShopItem.value = null;

      if (page === 'auth' && this.playerData == null) {
        currentPage.value = page;
      } else {
        // иначе, если игрок уже авторизован...
      }

      if (page === 'shop') {
        shopPage.value = donate.value.categories[0].path;
      }
    };

    const setShopCategory = (page) => {
      shopPage.value = page;
    };

    const setAuthMethod = (method) => {
      authMethod.value = method;

      login.value = null;
      password.value = null;
      repeatPassword.value = null;
      email.value = null;
      promo.value = null;
    };

    const updateSettingInputs = () => {
      for (let i = 0; i < chatSettings.length; i += 1) {
        chatSettings[i].model = settings.value.chat[chatSettings[i].name];
      }
    };

    const selectShopItem = (item) => {
      selectedShopItem.value = item;
    };

    const chunkList = (array) => {
      const result = [];
      const itemsPerArr = 12;

      for (let i = 0; i < array.length; i += 1) {
        const startIndex = i * itemsPerArr;
        const endIndex = i * itemsPerArr + itemsPerArr;

        if (i * itemsPerArr + itemsPerArr >= array.length) {
          result[i] = {
            items: array.slice(startIndex, array.length),
            id: i + 1,
          };

          break;
        } else {
          result[i] = {
            items: array.slice(startIndex, endIndex),
            id: i + 1,
          };
        }
      }

      return result;
    };

    watch(chat.value, () => {
      nextTick(() => {
        messagesContainer.value.scrollTop = messagesContainer.value.scrollHeight;
      });
    });

    watch(settings.value, updateSettingInputs);

    onMounted(updateSettingInputs);

    return {
      dropShowed,
      currentPage,
      login,
      password,
      repeatPassword,
      email,
      promo,
      authMethod,
      playerId,
      message,
      messagesContainer,
      settingsPage,
      chatSettings,
      shopPage,
      selectedShopItem,

      // store
      online,
      maxOnline,
      updates,
      playerData,
      headList,
      teams,
      chat,
      settings,
      gameModes,
      lobbies,
      donate,

      // methods
      changeHeadDropState,
      setPage,
      setAuthMethod,
      setSettingsPage,
      chunkList,
      setShopCategory,
      selectShopItem,
    };
  },
};
